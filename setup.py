"""Package setup file."""
from pathlib import Path

from setuptools import find_packages, setup

PROJECT_NAME = "VRChat Speech Assistant"
DESCRIPTION = (
    "The VRChat Speech Assistant allows you to easily use a speech to speech, "
    "with automatic translation. And / or speech to text with the VRChat "
    "chatbox using OSC protocol."
)
AUTHOR = "Amelien Deshams"
AUTHOR_EMAIL = "amelien.deshams@pm.me"
SCRIPT_PATH = Path(__file__).resolve().parent
README_FILEPATH = SCRIPT_PATH / "README.md"
REQUIREMENTS_FILEPATH = SCRIPT_PATH / "requirements.txt"
if README_FILEPATH.is_file():
    LONG_DESCRIPTION = README_FILEPATH.read_text(encoding="utf8")
else:
    LONG_DESCRIPTION = "Unable to load README.md"
if REQUIREMENTS_FILEPATH.is_file():
    REQUIREMENTS = REQUIREMENTS_FILEPATH.read_text(encoding="utf8").splitlines()
else:
    REQUIREMENTS = []
VERSION = (SCRIPT_PATH / "vrchat_speech_assistant" / "version.py").read_text().split('"')[1]

setup(
    name=PROJECT_NAME,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    project_urls={
        "Source": "https://gitlab.com/ameliend/vrchat-speech-assistant",
    },
    classifiers=[  # https://pypi.org/classifiers/
        "Development Status :: 5 - Production/Stable",
        "Environment :: Win32 (MS Windows)",
        "Intended Audience :: End Users/Desktops",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python :: 3.9",
        "Topic :: Utilities",
        "Topic :: Communications",
        "Topic :: Communications :: Chat",
        "Topic :: Multimedia :: Sound/Audio :: Sound Synthesis",
    ],
    packages=find_packages(exclude=["tests"]),
    license="MIT",
    python_requires=">=3.9",
    install_requires=REQUIREMENTS,
    include_package_data=True,
)
