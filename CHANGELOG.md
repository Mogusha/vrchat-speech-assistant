## [1.8.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.7.0...v1.8.0) (2023-07-24)


### ⏩ Performance

* removed dynamic energy threshold ([ffa5a51](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/ffa5a5133c95454380ca66a1733750874eeb1828))


### 📔 Docs

* update download link ([9ffc79b](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/9ffc79b56c706cc3dfd00296bfaf27082480c8b9))
* update download link ([f5da108](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/f5da108ff5b1723a61b1d0bebfd7a1def9c8ea6f))


### 🚀 Features

* added Japanese formality ([dfe626d](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/dfe626d2af565857f418cb4a4b93afa61ea0cb9d))
* new UI sounds ([0570f75](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/0570f75a7659cdcd497ca91b4ffd6c31a741a4aa))

## [1.7.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.6.0...v1.7.0) (2023-06-21)


### ⏩ Performance

* reset listener thread after every recognized text ([c60327a](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/c60327a52166f557a3a76783098cc4e75182f3ff))


### 📔 Docs

* update LICENSE ([8ca597d](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/8ca597d0a0e7e2bba0dccabdc52653be72eab557))


### 🦊 CI/CD

* update classifiers ([ac47aef](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/ac47aefa0e084de37d74c44df818cb1d4494f4d8))
* update requirements versions ([4472352](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/4472352522731c7508b56007d64990ffa9c0297c))


### 🚀 Features

* added more words to ignore ([22fa89e](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/22fa89e87266a7b1b133e6c47b8fca53c41e0a58))


### Other

* removed python2 coding args ([09dc115](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/09dc1158af307dcf3b81251faa80b99733836f35))
* update pylint config ([b68ca7f](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/b68ca7fb14c93ed0a5522df01e30de25d528e041))

## [1.6.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.5.0...v1.6.0) (2023-05-05)


### :scissors: Refactor

* separate functions in utils ([625bef2](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/625bef2d7bc4457c45a9892891644cda0f1218c6))


### ⏩ Performance

* refactoring of the code for better performances ([5542938](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/5542938f5a4df2cb6fb98db2b4a3b7664f1d8300))


### 📔 Docs

* udpate docs ([a5e4ecf](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/a5e4ecf7f0ab2d58c78b57ec4fc02a6567c99e4d))
* update README ([479b144](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/479b1447efa9a50c0d25c2686d13a64159b2e6fd))
* update README ([79e506b](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/79e506b519e2bc55994fbd7edf9c1c0743e8a6fa))
* update README ([dcb1b6d](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/dcb1b6d359c4f28fd61476b4ad28825217756c3e))
* update README ([94949ea](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/94949ea775f3a5aa20737168bf451d380f7131a7))
* update README ([c759e6f](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/c759e6f6159d60828f47e7a282928db0bc8cfc54))


### 🚀 Features

* add osc server to listen commands ([2adda2a](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/2adda2a15f753c71a6c6bba18ba019878d3e6a58))
* added Korean and Norwegian language support. ([fc06239](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/fc062393a3e1fabfc758c2eb3a030ebf1e7633c8))
* hide DeepL free api key and tests using get_usage method ([021fa70](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/021fa70188f031df538130af0b9f5ca2fac3d243))
* toggle recognizer beep sound ([73b6833](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/73b6833e2891ddc2c2c0d48d4b20b971cb7ed127))
* update pause_threshold and non_speaking_duration values ([547f874](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/547f87430638c95e21adf5ba5b5a8cd951d136e7))
* voice activity bar ([62d2126](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/62d21264f21f3792dc9bd28926821db083a2057c))


### 🛠 Fixes

* auto refresh devices on open if config mismatch ([14eb9a6](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/14eb9a6367a21bd0cdbe81207a67ae004c9ab5f1))
* ignore space for server receving osc message ([020655d](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/020655d9e06605df0c763e0d63546fffc78c98e1))
* refactoring of the management of audio devices ([d187c63](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/d187c638a7064cebb3532be3288429297ecc44b4))

## [1.5.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.4.0...v1.5.0) (2023-02-08)


### 📔 Docs

* update link ([0cc2491](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/0cc2491516c3f1e5a55efb5378f2a2e6f4fd6d50))


### 🦊 CI/CD

* update note. ([7a4bfdc](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/7a4bfdc311bff340eb676f5e09dd1bcd1b2e0e91))


### 🚀 Features

* add extra tags and refresh devices. ([bd7e785](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/bd7e785ba16ea89a8c77a314575c6e5d8c47c394))

## [1.4.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.3.2...v1.4.0) (2022-10-07)


### 📔 Docs

* update doc ([e663cbc](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/e663cbc41f21e2b5d47bb91e26af7c56b9b1f576))
* update doc ([d85dd35](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/d85dd355dcdf166c4ad0bff3b80a5932e965b0db))
* update download link ([36a2112](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/36a21123bf85497e070add30b6d1fe04a326673f))


### 🚀 Features

* added Ukrainian language support. ([07067c8](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/07067c878f505020229bee0303bac6d100cb5ca3))
* Replaced beep sound and typing animation. ([f249666](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/f2496667def011612ca04f80016441a72e50b8be))


### 🛠 Fixes

* disable trying to speech if there is no voice available for the speech. ([2bdffa9](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/2bdffa9b39a6cf6faef6cdad1416e0d9cef9f083))
* update free deepl api default key ([3b13b94](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/3b13b94a2401ba87b233a42b9c09d29f72a12cc2))
* wrong english Deepl source language attribute ([54fd9af](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/54fd9afcbd84c34323489e568ef28a0071c831cd))
* wrong english Deepl source language attribute (formality) ([5934eb4](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/5934eb4eb5a31ab1369a9659f9423ada71164c20))

## [1.3.2](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.3.1...v1.3.2) (2022-09-16)


### 📔 Docs

* correct spelling ([eec88d0](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/eec88d08bfaa5fbc5ac25b6269fd550fc20da646))
* update download link ([9851e40](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/9851e40f7dd708d9f9802b669c4566665a45717c))


### 🦊 CI/CD

* restore .pylintrc file ([3680110](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/368011032e15765ee271e833597e892fdbf76710))

## [1.3.1](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.3.0...v1.3.1) (2022-09-16)


### :scissors: Refactor

* correction of spelling errors ([38aac46](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/38aac46d9bb273f555733adc521a23b0f829f922))
* removal of unnecessary brackets ([4d502ce](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/4d502ce6c324c2bbb7c7bcd38f499ad91d8ab779))
* removed argument equals to the default parameter value ([f72a565](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/f72a5656db8e3e2e096bc6d85b3d1d7131162f8f))
* renaming of various variables ([fe3268b](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/fe3268ba230c4a636e9b7f33553ef0a94c8cbe0c))
* replaced atom badge with Pycharm ([b3db13e](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/b3db13e6ca9e72783394da910921fc94886037ab))


### 💈 Style

* replaced description line too long ([069ecef](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/069ecef077a26372c7462e1904d2e51f9703f474))


### 📔 Docs

* refactor docs ([58303c3](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/58303c332fe356cfa467111e8d8288cc1ecb7b42))


### 🦊 CI/CD

* removed pylintrc file ([1081a35](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/1081a35aeebb7d5fd30b9246dc15c80043939ea9))

## [1.3.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.2.0...v1.3.0) (2022-09-02)


### 📔 Docs

* add README documentation ([ef85177](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/ef8517771fae3065c332ef92fa88351c0d071236))


### 🚀 Features

* add free API keys ([e9e0f47](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/e9e0f4721a9fdabecc864913f6caf75047488aaf))

## [1.2.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.1.0...v1.2.0) (2022-08-31)


### :scissors: Refactor

* reformat code ([0f7c505](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/0f7c505580e6ea042965494898a984e707592f21))
* rename Portuguese Brazilian for using with args ([0c58c4f](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/0c58c4fe8b9017c595e9ed67327914a314b0880e))
* update recognizer beep ([67f9754](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/67f975424b4fa99e37236192931c618ccf243cd7))


### ⏩ Performance

* perf and tests improvement ([311975a](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/311975af695866734ee7432722855ba272d2c72f))


### 💈 Style

* add SCRIPT_PATH constant ([8cffced](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/8cffced9b0496aa2d5068880ea8c0ee6aa02732a))
* isort ([a0c6854](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/a0c68543204ea62c1659f82ff882612b79f22126))
* refactor ([3258c86](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/3258c8684e8f75b537590213ff1ae870ff82dd67))


### 🚀 Features

* add formality argument parametter ([361eb0c](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/361eb0c46bac2dcc472b7faaf29778300750ff6a))


### 🛠 Fixes

* Formality ([eacf9b2](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/eacf9b2cb14eacda1b797a00cf115e21cb2c6632))
* formality was not saved in ui ([2dd5561](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/2dd55617ffab2cb5e2566f49213da28119bc5875))
* Portuguese Brazilian renamed ([6c46c81](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/6c46c817263de318ed7e1e43228e8d929f7d4199))
* when saying stop send OSC typing False ([657ec25](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/657ec25347ff1859c606bb14a16c67292a817cfc))
* wrong formality parameter in basic configuration ([7da01b0](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/7da01b07de768f70cb1b3f2978126059c52def19))
* wrong language key for Japanese ([67e4631](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/67e4631f9260b31573fa8df21a56714c276b5550))

## [1.1.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.0.1...v1.1.0) (2022-08-28)


### :scissors: Refactor

* change log label widget color to orange ([ef1d1cb](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/ef1d1cb98260134fc721bcf38c9119e7bd033c63))
* reformat ([9a63039](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/9a6303976f7755a369b8e52df92f486721da2d31))
* set log level to INFO ([8eebce8](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/8eebce8e05dbf550758ffc5fe018ee2cc6e850f5))
* update log info "Listening" ([a85bfa2](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/a85bfa289dc5281d06ec2ce44d270b9f7cb13140))
* update log level info to warn ([784840c](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/784840c6fbea850189867789dc7e422ffcb70dc3))


### 🚀 Features

* add mic source input ([3ba7604](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/3ba76041224bd09b90660578db8d0c291d54f4ca))
* add on voice combo clicked ([aa12509](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/aa1250966cee96158483d470fdeafd0e6f0a044e))
* add tips label under the banner ([d7b457e](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/d7b457e8c27974cca9a3be4adbab6adc0d557850))
* add translator formality widget ([d779c87](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/d779c872b3e422c0dbc5dd0771ba1cc92211f9c8))
* set fixed size ([42751e2](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/42751e2bb743a268940ae89ecdc82d227ecda129))
* update args parser ([98709a0](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/98709a07f23bc79e28d2d92442bc80d0da70a81c))


### 🛠 Fixes

* remove margin top for groupbox ([777e5fc](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/777e5fc6b8a176b5885ec3c74dd1ea42e3ded174))
* update open repo link ([6c9a6fc](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/6c9a6fc8ff29526e7a984b5904535a83f4a95284))

## [1.0.1](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/v1.0.0...v1.0.1) (2022-08-26)


### 🛠 Fixes

* add MANIFEST file ([c1004d6](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/c1004d62abf0c37223f1ef79bb38703612517c71))
* wrong key ([eda0fe6](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/eda0fe65937dade8967bd05544325ab3fac5d06c))

## [1.0.0](https://gitlab.com/ameliend/vrchat-speech-assistant/compare/...v1.0.0) (2022-08-26)


### 🚀 Features

* append logo ico ([9232d29](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/9232d291458b888a804fad1e6f4b661e33de52e2))
* initial commit ([18a9001](https://gitlab.com/ameliend/vrchat-speech-assistant/commit/18a9001b9b1d1b783ef1a130b3be9f454dec322d))
