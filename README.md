[![pipeline status](https://gitlab.com/ameliend/vrchat-speech-assistant/badges/main/pipeline.svg)](https://gitlab.com/ameliend/vrchat-speech-assistant/-/commits/main)
[![pylint](https://gitlab.com/ameliend/vrchat-speech-assistant/-/jobs/artifacts/main/raw/public/badges/pylint.svg?job=pylint)](https://gitlab.com/ameliend/vrchat-speech-assistant/-/commits/main)
[![vscode-editor](https://badgen.net/badge/icon/visualstudio?icon=visualstudio&label)](https://code.visualstudio.com/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

# VRChat Speech Assistant

<p align="center">
  <img src="./vrchat_speech_assistant/resources/banner.png">
</p>

The VRChat Speech Assistant allows you to use a **speech to speech, with automatic translation.** 
And / or speech to text with the **VRChat chatbox** using OSC protocol.

## ✅ [How to use (Video Tutorial) Click here!](https://youtu.be/NQgI6AiNOs8)

## ⏬ [Download the latest release here!](https://drive.proton.me/urls/2PYYRWYA18#h7vkjUV9RiQt)

#### 📦 [Older releases here](https://drive.proton.me/urls/ENCGB10WWR#XcsiRyzLc4BT)

## 🌐 **It works 100% ONLINE.** 

- You don't need to download an offline model.
- It won't use your CPU or GPU.

## 🚀 **It is FAST and ACURATE.** 

- The application uses Google Cloud Speech-to-Text API, it is constantly updated.

## 🔣 **TRANSLATE over 30+ languages.** 

- The translation is provided by DeepL, the best translator to date.

## 🔧 Configuration

<p align="center">
  <img src="./vrchat_speech_assistant/resources/screenshot.png">
</p>

1. Select a **Source language**, it is the language in which you speak for voice recognition.
2. Select a **Target language**, it is the language in which you want to speak,
   if the target language is different from the source language then the translation will be automatic.
3. Select **Speech** if you want to output the result of the recognizer to the desired audio output.
   Then select an **Input** and **Output** devices. For the speech to be transmitted to a microphone source,
   you need to install a **Virtual Cable** first, and select the audio output to **Cable Input**.
4. Select the **Voice**, Select the voice, it is linked to the target language.
   Sometimes no voices are available for the target language, in this case only the **Chatbox** will be
   available.
5. Select **VRChat Chatbox** if you want to output into a text your recognition to the VRChat Chatbox.
   (Requires activation of the OSC feature in VRChat)

## 🔌 Virtual Cable setup

To be able to read your speech in VRChat, you will have to install a virtual cable first.

https://vb-audio.com/Cable/

Then select it from the input device.

<p align="center">
  <img src="./vrchat_speech_assistant/resources/device.png">
</p>

## 💬 VRChat Chatbox setup (OSC)

Make sure your OSC feature is enabled in VRChat.

<p align="center">
  <img src="./vrchat_speech_assistant/resources/activate.jpg">
</p>

And that's it, no need for additional configuration.


## ✨ Super-duper cool alternatives

If you are looking for alternatives, here are some similar applications 
that provide other great features!

[Whispering Tiger (Live Translate/Transcribe)](https://github.com/Sharrnah/whispering)

[TTS-Voice-Wizard](https://github.com/VRCWizard/TTS-Voice-Wizard)

## 🧨 Feature request of find a bug?

You can contact me on Discord nkosan, or better, create a ticket in this repository!
