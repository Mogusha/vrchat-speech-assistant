"""Providers classes that can be used as speech_provider attribute for the VRCSpeechAssistant class."""
import configparser
import contextlib
import importlib
import re
import threading
from os import getenv
from pathlib import Path

import boto3
import botocore
import botocore.exceptions
import sounddevice
import soundfile

from logger import debug, error, info

CURRENT_FRAME = 0
event = threading.Event()


class Polly:
    """Use Amazon Polly API to synthesize a speech.

    This class can be used as speech_provider attribute for the VRCSpeechAssistant class.

    Attributes
    ----------
    AWS_CONFIG_FILE: Path
        Default location of Amazon AWS API config file.
    AWS_CREDENTIALS_FILE: Path
        Default location of Amazon AWS API credentials file.
    VOICES_PATH: Path
        The location of the voices files that will be generated.
    """

    AWS_CONFIG_FILE = Path.home() / ".aws" / "config"
    AWS_CREDENTIALS_FILE = Path.home() / ".aws" / "credentials"
    VOICES_PATH = Path(getenv("APPDATA")) / "VRChat Speech Assistant" / "voices"

    def __init__(self):
        """Initialize instance."""
        self.aws_config = configparser.ConfigParser()
        self.aws_credentials = configparser.ConfigParser()
        if not self.AWS_CONFIG_FILE.exists() or not self.AWS_CREDENTIALS_FILE.exists():
            self._create_base_config()
        self.aws_config.read(self.AWS_CONFIG_FILE)
        self.aws_credentials.read(self.AWS_CREDENTIALS_FILE)
        self.client = boto3.client("polly")
        # The first self.client.synthesize_speech called takes a little time to respond,
        # calling self.speech once before the user call it.
        self.speech("test", "Justin", 999)

    def _create_base_config(self):
        self.AWS_CONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
        self.aws_config["default"] = {"region": "us-west-2"}
        self.aws_config.write(self.AWS_CONFIG_FILE.open("w", encoding="utf-8"))
        self.aws_credentials["default"] = {
            "aws_access_key_id": "AKIAQGMSD6S45KT5J7PK",
            "aws_secret_access_key": "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",
        }
        self.aws_credentials.write(self.AWS_CREDENTIALS_FILE.open("w", encoding="utf-8"))

    def speech(self, text: str, voice_id: str, output_device_index: int, extra_tags: dict = None):
        """Convert a text into a speech and then play it to the given output_device.

        Parameters
        ----------
        text : str
            The text to be converted to speech.
        voice_id : str
            The voice id of the voice you want to use.
        output_device_index : int
            The index of the output device.
        extra_tags : dict
            A dict of extra tags. Currently only {"Whispered": bool} available.
        """

        def convert_to_ssml(input_text):
            # https://docs.aws.amazon.com/polly/latest/dg/supportedtags.html
            if extra_tags and extra_tags.get("whispered"):
                return (
                    "<speak>"
                    + '<amazon:auto-breaths frequency="low" volume="soft" duration="x-short">'
                    + '<amazon:effect name="whispered">'
                    + input_text
                    + "</amazon:effect>"
                    + "</amazon:auto-breaths>"
                    + "</speak>"
                )
            return (
                "<speak>"
                + '<amazon:auto-breaths frequency="low" volume="soft" duration="x-short">'
                + input_text
                + "</amazon:auto-breaths>"
                + "</speak>"
            )

        # Remove special characters from the text for the file destination.
        filename = re.sub("[#<$+%>!`&*'|{?\"=/}:@]", "", text)
        if extra_tags and extra_tags.get("whispered"):
            filepath = self.VOICES_PATH / voice_id / f"w_{filename}.ogg"
        else:
            filepath = self.VOICES_PATH / voice_id / f"{filename}.ogg"
        # It creates the directory if it doesn't exist.
        filepath.parent.mkdir(parents=True, exist_ok=True)
        # Create the answer from Amazon Polly if the file does not exist.
        if not filepath.exists():
            try:
                response = self.client.synthesize_speech(
                    Text=convert_to_ssml(text),
                    VoiceId=voice_id.split(" ")[0],
                    OutputFormat="ogg_vorbis",
                    TextType="ssml",
                )
            except botocore.exceptions.ClientError as err:
                error(err)
                return
            with open(filepath, "wb") as file:
                file.write(response["AudioStream"].read())
        read_audio_file(str(filepath), output_device_index)


def test_amazon_polly() -> bool:
    """Test Amazon Polly credentials.

    Returns
    -------
    bool
        The test result.
    """
    with contextlib.suppress(botocore.exceptions.ClientError):
        importlib.reload(boto3)
        test_polly = boto3.client("polly")
        test_polly.synthesize_speech(
            Text="test",
            VoiceId="Justin",
            OutputFormat="ogg_vorbis",
        )
        return True
    return False


def read_audio_file(filepath: str, output_device_index: int):
    """Read an audio file and plays it on the specified output device.

    Parameters
    ----------
    filepath : str
        The path to the audio file to be played.
    output_device_index : int
        The index of the output device to use.
    """

    def callback(data_out, frames, _, status):
        global CURRENT_FRAME
        if status:
            debug("status: ", status)
        chunk_size = min(len(data) - CURRENT_FRAME, frames)
        data_out[:chunk_size] = data[CURRENT_FRAME : CURRENT_FRAME + chunk_size]
        if chunk_size < frames:
            data_out[chunk_size:] = 0
            CURRENT_FRAME = 0
            raise sounddevice.CallbackStop()
        CURRENT_FRAME += chunk_size

    def stream_thread():
        with stream:
            event.wait()  # Wait until playback is finished
        event.clear()

    data, samplerate = soundfile.read(filepath, always_2d=True)
    # Creating a thread to play the audio file.
    try:
        stream = sounddevice.OutputStream(
            samplerate=samplerate,
            device=output_device_index,
            channels=data.shape[1],
            callback=callback,
            finished_callback=event.set,
        )
    except sounddevice.PortAudioError:
        return
    info("Reading audio")
    thread = threading.Thread(target=stream_thread)
    thread.start()
