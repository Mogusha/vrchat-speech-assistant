from typing import Optional

import deepl
import sounddevice


def get_input_devices() -> list:
    """Return a list of compatible input devices.

    Returns
    -------
    list
        A list of sounddevice dict.
    """
    return [
        {"index": index, "device": item}
        for index, item in enumerate(sounddevice.query_devices())
        if item["max_output_channels"] == 0
        and item["hostapi"] == 0
        and "Microsoft Sound Mapper" not in item["name"]
    ]


def get_output_devices() -> list:
    """Return a list of compatible output devices.

    Returns
    -------
    list
        A list of sounddevice dict.
    """
    return [
        {"index": index, "device": item}
        for index, item in enumerate(sounddevice.query_devices())
        if item["max_input_channels"] == 0
        and item["hostapi"] == 0
        and "Microsoft Sound Mapper" not in item["name"]
    ]


def test_deepl(api_key: str) -> Optional[deepl.Translator]:
    """The function tests if a DeepL API key is valid by checking the usage limit.

    Parameters
    ----------
    api_key : str
        The DeepL API key.

    Returns
    -------
    deepl.Translator or None
        Return the DeepL translator object if the test is passed, None otherwise.
    """
    if not api_key:
        return None
    try:
        translator = deepl.Translator(api_key)
        translator.get_usage()
        return translator
    except (deepl.exceptions.AuthorizationException, deepl.exceptions.DeepLException):
        return None
