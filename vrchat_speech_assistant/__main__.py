"""The VRChat Speech Assistant allows you to easily use a speech to speech, with automatic translation.

And / or speech to text with the VRChat chatbox using OSC protocol.
"""
import configparser
import contextlib
import re
import sys
import webbrowser
import winsound
from os import getenv
from pathlib import Path
from threading import Thread

import deepl
import sounddevice
import speech_recognition
from logger import info, warn
from PySide6 import QtGui, QtWidgets
from PySide6.QtCore import Qt
from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import BlockingOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient

from vrchat_speech_assistant import darkstyle, languages, providers, utils
from vrchat_speech_assistant.config import DEEPL_API_KEY

CONFIG_FILE = Path(getenv("APPDATA")).joinpath("VRChat Speech Assistant", "config.ini")
RESOURCES_PATH = Path(__file__).resolve().parent.joinpath("resources")

dispatcher = Dispatcher()


class VRCSpeechAssistantWindow(QtWidgets.QWidget):
    """The main window of the VRC Speech Assistant application.

    Using the parameters chosen by the user, this application allows you to use a
    speech to speech, or speech to text, or both, with your chosen provider.
    You can also use a translator to auto translate your sentence using DeepL.

    Note
    ----
        - The recognizer uses Google services, no configuration needed.
        - Amazon Polly need an API key, but a default one is provided.
        - DeepL translator need an API key, but a default one is provided.

    Attributes
    ----------
    speech_provider: object
        The selected speech provider for the speech engine.
    extra_tags: dict
        Optional extra tags to be provided to the speech provider object.
    client: SimpleUDPClient
        The OSC client for VRChat on port 9000.
    listener: speech_recognition.Recognizer.listen_in_background
        The listener object when the listener thread is running.
    is_listener_running: bool
        The state of the listener thread.
    input_devices: list
        A list of input sounddevice dict.
    output_devices: list
        A list of input sounddevice dict.
    config: configparser.ConfigParser
        The configuration file parser.
    """

    def __init__(self):
        """Constructor for VRCSpeechAssistantWindow."""
        super().__init__()
        self.speech_provider = None
        self.extra_tags = {}
        self.osc_client = SimpleUDPClient("127.0.0.1", 9000)
        self.osc_server = BlockingOSCUDPServer(("127.0.0.1", 7001), dispatcher)
        self.listener = None
        self.is_listener_running = False
        self.input_devices = utils.get_input_devices()
        self.output_devices = utils.get_output_devices()
        self.config = configparser.ConfigParser(comment_prefixes="/", allow_no_value=True)
        self._init_config()
        self._init_ui()
        self._init_translator()
        self._init_server()

    def _init_config(self):
        if not CONFIG_FILE.exists():
            self._create_base_config()
        self.config.read(CONFIG_FILE)

    def _create_base_config(self):
        CONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
        self.config["DEFAULT"] = {"speech": True, "beep": True, "chatbox": True}
        self.config["API"] = {"deepl_api_key": ""}
        self.config["SPEECH"] = {
            "source_language": "English",
            "target_language": "English",
            "provider": "Amazon Polly",
            "voice": "Justin",
            "input_device": self.input_devices[0]["device"]["name"],
            "output_device": self.output_devices[0]["device"]["name"],
        }
        self.config["TRANSLATOR"] = {"split_sentences": "1", "formality": "Default"}
        self.save_config()

    def _init_ui(self):
        self.setWindowFlag(Qt.Window)
        self.setWindowTitle("VRChat Speech Assistant")
        self.setWindowIcon(QtGui.QIcon(str(RESOURCES_PATH.joinpath("logo.png"))))
        self.setFixedSize(425, 900)
        # Menubar.
        menu_bar = QtWidgets.QMenuBar(self)
        file_menu = menu_bar.addMenu("File")
        file_open_voices_folder = file_menu.addAction("Open voices folder", self.open_voices_folder)
        file_open_voices_folder.setIcon(QtGui.QIcon(str(RESOURCES_PATH.joinpath("folder.png"))))
        edit_menu = menu_bar.addMenu("Edit")
        edit_configuration_action = edit_menu.addAction("Configuration", self.edit_config)
        edit_configuration_action.setIcon(QtGui.QIcon(str(RESOURCES_PATH.joinpath("edit.png"))))
        edit_refresh_devices_action = edit_menu.addAction("Refresh devices", self.refresh_devices)
        edit_refresh_devices_action.setIcon(QtGui.QIcon(str(RESOURCES_PATH.joinpath("refresh.png"))))
        more_menu = menu_bar.addMenu("More")
        open_web_action = more_menu.addAction("Open Gitlab Repository", self.open_repository)
        open_web_action.setIcon(QtGui.QIcon(str(RESOURCES_PATH.joinpath("web.png"))))
        # Main layout.
        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.setSpacing(10)
        main_layout.setContentsMargins(30, 30, 30, 30)
        # Banner.
        banner = QtWidgets.QLabel(self)
        banner.setPixmap(QtGui.QPixmap(str(RESOURCES_PATH.joinpath("banner.png"))))
        banner.setAlignment(Qt.AlignCenter)
        tips_label = QtWidgets.QLabel(
            "It is best to articulate well and avoid any source of background noise."
        )
        # Source language widget.
        source_language_label = QtWidgets.QLabel("Source language:")
        self.source_language_combo = QtWidgets.QComboBox()
        self.source_language_combo.addItems(languages.GOOGLE.keys())
        self.source_language_combo.setCurrentText(self.config["SPEECH"]["source_language"])
        self.source_language_combo.currentTextChanged.connect(self._on_source_language_combo_clicked)
        # Target language widget.
        target_language_label = QtWidgets.QLabel("Target language:")
        self.target_language_combo = QtWidgets.QComboBox()
        self.target_language_combo.addItems(languages.DEEPL.keys())
        self.target_language_combo.setCurrentText(self.config["SPEECH"]["target_language"])
        self.target_language_combo.currentTextChanged.connect(self._on_target_language_combo_clicked)
        # Formality widget.
        formality_label = QtWidgets.QLabel("Formality (if available):")
        self.formality_combo = QtWidgets.QComboBox()
        self.formality_combo.addItems(["Default", "Formal", "Informal"])
        self.formality_combo.setCurrentText(self.config["TRANSLATOR"]["formality"])
        self.formality_combo.currentTextChanged.connect(self._on_formality_combo_clicked)
        # Speech groupbox widget.
        self.speech_status_checkbox = QtWidgets.QCheckBox("Speech")
        self.speech_status_checkbox.setChecked(self.config["DEFAULT"].getboolean("speech"))
        self.speech_status_checkbox.stateChanged.connect(self._on_speech_status_checkbox_clicked)
        self.speech_groupbox = QtWidgets.QGroupBox(self)
        self.speech_groupbox.setEnabled(self.config["DEFAULT"].getboolean("speech"))
        speech_groupbox_layout = QtWidgets.QVBoxLayout(self.speech_groupbox)
        # Speech provider widget.
        speech_provider_label = QtWidgets.QLabel("Speech provider:")
        self.speech_provider_combo = QtWidgets.QComboBox()
        self.speech_provider_combo.addItems(["Amazon Polly"])
        self.speech_provider_combo.setCurrentText(self.config["SPEECH"]["provider"])
        self.speech_provider_combo.currentTextChanged.connect(self._on_speech_provider_combo_clicked)
        # Extra tags widgets.
        extra_tags_label = QtWidgets.QLabel("Extra tags:")
        self.extra_tags_combo = QtWidgets.QComboBox()
        self.extra_tags_combo.addItems(["None", "Whispered"])
        self.extra_tags_combo.currentTextChanged.connect(self._on_extra_tags_combo_clicked)
        # Speech voices widget.
        speech_voice_label = QtWidgets.QLabel("Voice:")
        self.speech_voice_combo = QtWidgets.QComboBox()
        # Speech input widget.
        input_device_label = QtWidgets.QLabel("Input device:")
        self.input_device_combo = QtWidgets.QComboBox()
        input_device_names = [item["device"]["name"] for item in self.input_devices]
        self.input_device_combo.addItems(input_device_names)
        saved_input_device = self.config["SPEECH"].get("input_device")
        if saved_input_device and saved_input_device in input_device_names:
            self.input_device_combo.setCurrentText(saved_input_device)
        else:
            self.config["SPEECH"]["input_device"] = input_device_names[0]
            self.save_config()
        self.input_device_combo.currentTextChanged.connect(self._on_input_device_clicked)
        # Speech output widget.
        output_device_label = QtWidgets.QLabel("Output device:")
        self.output_device_combo = QtWidgets.QComboBox()
        output_device_names = [item["device"]["name"] for item in self.output_devices]
        self.output_device_combo.addItems(output_device_names)
        saved_output_device = self.config["SPEECH"].get("output_device")
        if saved_output_device and saved_output_device in output_device_names:
            self.output_device_combo.setCurrentText(saved_output_device)
        else:
            self.config["SPEECH"]["output_device"] = output_device_names[0]
            self.save_config()
        self.output_device_combo.currentTextChanged.connect(self._on_output_device_clicked)
        # Add widgets to speech groupbox widget.
        speech_groupbox_layout.addWidget(speech_provider_label)
        speech_groupbox_layout.addWidget(self.speech_provider_combo)
        speech_groupbox_layout.addWidget(extra_tags_label)
        speech_groupbox_layout.addWidget(self.extra_tags_combo)
        speech_groupbox_layout.addWidget(speech_voice_label)
        speech_groupbox_layout.addWidget(self.speech_voice_combo)
        speech_groupbox_layout.addWidget(input_device_label)
        speech_groupbox_layout.addWidget(self.input_device_combo)
        speech_groupbox_layout.addWidget(output_device_label)
        speech_groupbox_layout.addWidget(self.output_device_combo)
        # OSC Chatbox widget.
        self.chatbox_status_checkbox = QtWidgets.QCheckBox("VRChat Chatbox")
        self.chatbox_status_checkbox.setChecked(self.config["DEFAULT"].getboolean("chatbox"))
        self.chatbox_status_checkbox.stateChanged.connect(self._on_chatbox_status_clicked)
        # Beep option widget.
        self.beep_status_checkbox = QtWidgets.QCheckBox("Beep while recognizing")
        self.beep_status_checkbox.setChecked(self.config["DEFAULT"].getboolean("beep"))
        self.beep_status_checkbox.stateChanged.connect(self._on_beep_option_changed)
        # Activate button.
        self.toggle_activate_button = QtWidgets.QPushButton("Enable", self)
        self.toggle_activate_button.clicked.connect(self._on_toggle_activate_button_clicked)
        self.toggle_activate_button.clicked.connect(self.toggle_thread)
        # Log widget.
        self.log_line_edit = QtWidgets.QLabel(self)
        self.log_line_edit.setStyleSheet("color: orange")
        # Progressbar.
        self.progressbar = QtWidgets.QProgressBar(self)
        self.progressbar.setStyleSheet("border-style: none")
        self.progressbar.setGeometry(0, 890, 425, 10)
        # Add widgets to main layout.
        main_layout.setMenuBar(menu_bar)
        main_layout.addWidget(banner)
        main_layout.addWidget(tips_label)
        main_layout.addWidget(source_language_label)
        main_layout.addWidget(self.source_language_combo)
        main_layout.addWidget(target_language_label)
        main_layout.addWidget(self.target_language_combo)
        main_layout.addWidget(formality_label)
        main_layout.addWidget(self.formality_combo)
        main_layout.addWidget(self.speech_status_checkbox)
        main_layout.addWidget(self.speech_groupbox)
        main_layout.addWidget(self.chatbox_status_checkbox)
        main_layout.addWidget(self.beep_status_checkbox)
        main_layout.addWidget(self.toggle_activate_button)
        main_layout.addWidget(self.log_line_edit)
        # Append voices for the current provider.
        self._append_voices_for_selected_provider()
        self.speech_voice_combo.setCurrentText(self.config["SPEECH"]["voice"])
        self.speech_voice_combo.currentTextChanged.connect(self._on_voice_combo_clicked)
        # Checking which speech provider to use.
        if self.config["DEFAULT"].getboolean("speech"):
            self._check_selected_speech_provider()
        self.toggle_activate_button.setFocus()

    def _init_translator(self):
        api_key = self.config["API"].get("deepl_api_key") or DEEPL_API_KEY
        self.translator = utils.test_deepl(api_key)
        if not self.translator:
            self._disable_target_language_combo()

    def _init_server(self):
        def thread_server():
            with contextlib.suppress(OSError):
                self.osc_server.serve_forever()

        dispatcher.map("/source_language", self._server_change_source_language)
        dispatcher.map("/target_language", self._server_change_target_language)
        dispatcher.map("/formality", self._server_change_formality)
        dispatcher.map("/speech", self._server_toggle_speech)
        dispatcher.map("/speech_provider", self._server_change_speech_provider)
        dispatcher.map("/tag", self._server_change_extra_tags)
        dispatcher.map("/voice", self._server_change_voice)
        dispatcher.map("/chatbox", self._server_toggle_chatbox)
        dispatcher.map("/beep", self._server_toggle_recognizer_beep)
        dispatcher.map("/enabled", self._server_toggle_thread)
        thread = Thread(target=thread_server)
        thread.start()

    def _server_change_source_language(self, _, language):
        self.source_language_combo.setCurrentText(language)

    def _server_change_target_language(self, _, language):
        self.target_language_combo.setCurrentText(language)

    def _server_change_formality(self, _, formality):
        self.formality_combo.setCurrentText(formality)

    def _server_toggle_speech(self, _, option):
        if option == 0:
            self.speech_status_checkbox.setChecked(False)
            return
        self.speech_status_checkbox.setChecked(True)

    def _server_change_speech_provider(self, _, provider):
        for item in range(self.speech_provider_combo.count()):
            item_text = self.speech_provider_combo.itemText(item)
            if item_text.startswith(provider):
                self.speech_provider_combo.setCurrentText(item_text)
                return

    def _server_change_extra_tags(self, _, tag):
        self.extra_tags_combo.setCurrentText(tag)

    def _server_change_voice(self, _, voice):
        for item in range(self.speech_voice_combo.count()):
            item_text = self.speech_voice_combo.itemText(item)
            if item_text.startswith(voice):
                self.speech_voice_combo.setCurrentText(item_text)
                return

    def _server_toggle_chatbox(self, _, option):
        if option == 0:
            self.chatbox_status_checkbox.setChecked(False)
            return
        self.chatbox_status_checkbox.setChecked(True)

    def _server_toggle_recognizer_beep(self, _, option):
        if option == 0:
            self.beep_status_checkbox.setChecked(False)
            return
        self.beep_status_checkbox.setChecked(True)

    def _server_toggle_thread(self, _, option):
        if option == 0 and self.is_listener_running:
            self.toggle_thread()
            self.toggle_activate_button.setText("Enable")
        elif option == 1 and not self.is_listener_running:
            self.toggle_thread()
            self.toggle_activate_button.setText("Disable")

    def _on_source_language_combo_clicked(self, current_text: str):
        self.config["SPEECH"]["source_language"] = current_text
        self.save_config()
        # Disable target language combo if translator attribute is None.
        if not self.translator:
            self._disable_target_language_combo()

    def _on_target_language_combo_clicked(self, current_text: str):
        self.config["SPEECH"]["target_language"] = current_text
        self.save_config()
        # Append voices for the current provider.
        self._append_voices_for_selected_provider()

    def _on_formality_combo_clicked(self, current_text: str):
        self.config["TRANSLATOR"]["formality"] = current_text
        self.save_config()

    def _on_speech_status_checkbox_clicked(self, checked: int):
        if checked == 2:
            self.speech_groupbox.setEnabled(True)
            self.config["DEFAULT"]["speech"] = "True"
            # Checking which speech provider to use.
            self._check_selected_speech_provider()
        else:
            self.speech_groupbox.setEnabled(False)
            if self.chatbox_status_checkbox.checkState() == Qt.CheckState.Unchecked:
                self.toggle_activate_button.setEnabled(False)
            self.speech_provider = None
            self.config["DEFAULT"]["speech"] = "False"
        self.save_config()

    def _on_speech_provider_combo_clicked(self, current_text: str):
        self.config["SPEECH"]["provider"] = current_text
        self.save_config()
        # Checking which speech provider to use.
        self._check_selected_speech_provider()

    def _on_extra_tags_combo_clicked(self, current_text: str):
        self.extra_tags = {"whispered": True} if current_text == "Whispered" else {}

    def _on_voice_combo_clicked(self, current_text: str):
        self.config["SPEECH"]["voice"] = current_text
        self.save_config()

    def _on_input_device_clicked(self, current_text: str):
        self.config["SPEECH"]["input_device"] = current_text
        self.save_config()

    def _on_output_device_clicked(self, current_text: str):
        self.config["SPEECH"]["output_device"] = current_text
        self.save_config()

    def _on_chatbox_status_clicked(self, checked: int):
        if checked == 2:
            self.log_line_edit.clear()
            self.config["DEFAULT"]["chatbox"] = "True"
            self.toggle_activate_button.setEnabled(True)
            # Checking which speech provider to use.
            if self.config["DEFAULT"].getboolean("speech"):
                self._check_selected_speech_provider()
        else:
            self.config["DEFAULT"]["chatbox"] = "False"
            # Disable the toggle activate button if the speech status checkbox is unchecked.
            if self.speech_status_checkbox.checkState() == Qt.CheckState.Unchecked:
                self.toggle_activate_button.setEnabled(False)
        self.save_config()

    def _on_beep_option_changed(self, checked: int):
        self.config["DEFAULT"]["beep"] = "True" if checked == 2 else "False"
        self.save_config()

    def _on_toggle_activate_button_clicked(self):
        if not self.is_listener_running:
            self.toggle_activate_button.setText("Disable")
        else:
            self.toggle_activate_button.setText("Enable")

    def _disable_target_language_combo(self):
        self.log_line_edit.setText("DeepL API key is invalid or account usage limit reached.")
        self.target_language_combo.setEnabled(False)
        # Setting the target language to the same as the source language.
        self.target_language_combo.setCurrentText(self.source_language_combo.currentText())
        self.config["SPEECH"]["target_language"] = self.source_language_combo.currentText()
        self.save_config()

    def _check_selected_speech_provider(self):
        # Amazon Polly.
        if self.config["SPEECH"]["provider"] == "Amazon Polly":
            self.speech_provider = providers.Polly()
            if providers.test_amazon_polly():
                self.toggle_activate_button.setEnabled(True)
                self.log_line_edit.clear()
            else:
                self.speech_provider = None
                self.toggle_activate_button.setEnabled(False)
                self.log_line_edit.setText("Amazon AWS credentials are invalid or missing.")

    def _append_voices_for_selected_provider(self):
        self.speech_voice_combo.clear()
        # Amazon Polly.
        if self.config["SPEECH"]["provider"] == "Amazon Polly":
            for language, voices in languages.VOICES["polly"].items():
                if language == self.target_language_combo.currentText():
                    if not voices:
                        self.speech_voice_combo.addItem("None")
                        return
                    for voice in voices:
                        self.speech_voice_combo.addItem(voice)

    def _voice_activity(self, activity: bool):
        chatbox_enabled = self.config["DEFAULT"].getboolean("chatbox")
        if activity:
            self.progressbar.setMaximum(0)
            if chatbox_enabled:
                self.osc_client.send_message("/chatbox/typing", True)
            return
        self.progressbar.setMaximum(100)
        if chatbox_enabled:
            self.osc_client.send_message("/chatbox/typing", False)

    @staticmethod
    def open_voices_folder():
        """Open the folder containing all the generated voices."""
        voices_folder = CONFIG_FILE.parent.joinpath("voices")
        voices_folder.mkdir(parents=True, exist_ok=True)
        webbrowser.open(str(voices_folder))

    @staticmethod
    def open_repository():
        """Open the repository of the current script in your default web browser."""
        webbrowser.open("https://gitlab.com/ameliend/vrchat-speech-assistant")

    def edit_config(self):
        """Open the configuration dialog."""
        config_dialog = ConfigDialog(self)
        config_dialog.exec()

    def save_config(self):
        """Save configuration changes to the configuration file."""
        with CONFIG_FILE.open("w", encoding="utf-8") as config_file:
            self.config.write(config_file)

    def refresh_devices(self):
        """Refresh the list of available audio devices."""
        sounddevice._terminate()
        sounddevice._initialize()
        self.input_device_combo.clear()
        self.input_devices = utils.get_input_devices()
        self.input_device_combo.addItems([item["device"]["name"] for item in self.input_devices])
        self.output_device_combo.clear()
        self.output_devices = utils.get_output_devices()
        self.output_device_combo.addItems([item["device"]["name"] for item in self.output_devices])

    def toggle_thread(self):
        """Start or stop the listener thread."""
        recognizer = speech_recognition.Recognizer()
        input_device = self.config["SPEECH"]["input_device"]
        input_device_index = None
        for item in self.input_devices:
            if item["device"]["name"] == input_device:
                input_device_index = item["index"]
        if not input_device_index:
            return
        microphone = speech_recognition.Microphone(device_index=input_device_index)
        recognizer.pause_threshold = 0.5
        recognizer.non_speaking_duration = 0.5
        recognizer.dynamic_energy_threshold = False
        if not self.is_listener_running:
            self.is_listener_running = True
            # with microphone as source:
            #     recognizer.adjust_for_ambient_noise(source)
            info("Listening...")
            winsound.PlaySound("resources\\on.wav", winsound.SND_ASYNC)
            self.listener = recognizer.listen_in_background(microphone, self.recognize)
        else:
            self.is_listener_running = False
            winsound.PlaySound("resources\\off.wav", winsound.SND_ASYNC)
            self.listener(wait_for_stop=False)  # Stop the listener.
            info("Listener stopped.")

    def recognize(self, recognizer, audio):
        """Use Google's Speech Recognition API to recognize an audio file into a text string.

        Translate and speech the text if the attributes translator and / or speech_provider
        are not None.

        Note
        ----
        This method is intended to be used as an argument for the
        speech_recognition.Recognizer().listen_in_background function.

        Parameters
        ----------
        recognizer
            The speech recognizer object.
        audio
            The audio data to recognize.
        """
        self._voice_activity(True)
        source_language = self.config["SPEECH"]["source_language"]
        target_language = self.config["SPEECH"]["target_language"]
        if beep := self.config["DEFAULT"].getboolean("beep"):
            winsound.PlaySound("resources\\recognizing.wav", winsound.SND_ASYNC)
        try:
            text = recognizer.recognize_google(audio, language=languages.GOOGLE[source_language])
        except speech_recognition.UnknownValueError:
            warn("The voice recognition did not understand.")
            if beep:
                winsound.PlaySound("resources\\warn.wav", winsound.SND_ASYNC)
            self._voice_activity(False)
            return
        except speech_recognition.RequestError:
            warn("Could not request results from Google Speech Recognition service")
            if beep:
                winsound.PlaySound("resources\\warn.wav", winsound.SND_ASYNC)
            self._voice_activity(False)
            return
        if text.lower() in ("stop", "stoppe", "staff"):
            warn(f'Word "{text}" was ignored')
            if beep:
                winsound.PlaySound("resources\\warn.wav", winsound.SND_ASYNC)
            self._voice_activity(False)
            return
        info("Recognized:", text)
        if beep:
            winsound.PlaySound("resources\\recognized.wav", winsound.SND_ASYNC)
        # Translating the text if the attribute translator is not None,
        # and if the source language is different from the target language.
        if self.translator and source_language != target_language:
            text = self.translate(text)
        # Speech the text with the attribute function speech_provider.speech,
        # if there is a provider, and if there is a voice available to speech.
        voice = self.config["SPEECH"]["voice"]
        output_device = self.config["SPEECH"]["output_device"]
        output_device_index = None
        for item in self.output_devices:
            if item["device"]["name"] == output_device:
                output_device_index = item["index"]
        if not output_device_index:
            return
        if self.speech_provider and voice != "None":
            self.speech_provider.speech(text, voice, output_device_index, self.extra_tags)
        self._voice_activity(False)
        if self.config["DEFAULT"].getboolean("chatbox"):
            self.osc_client.send_message("/chatbox/input", [text, True])

    def translate(self, text: str) -> str:
        """Use DeepL's API to translate a text.

        Parameters
        ----------
        text : str
            The text to be translated.

        Returns
        -------
        str
            The translated text.
        """
        source_language = languages.DEEPL[self.config["SPEECH"]["source_language"]].split("-")[0]
        target_language = languages.DEEPL[self.config["SPEECH"]["target_language"]]
        split_sentences = self.config["TRANSLATOR"]["split_sentences"]
        formality = self.config["TRANSLATOR"]["formality"]
        formality_mapping = {"Default": "default", "Formal": "prefer_more", "Informal": "prefer_less"}
        translated_text = self.translator.translate_text(
            text,
            split_sentences=split_sentences,
            formality=formality_mapping[formality],
            source_lang=source_language,
            target_lang=target_language,
        ).text
        info("translated: ", translated_text)
        return translated_text

    def closeEvent(self, event):
        self.osc_server.server_close()
        event.accept()  # let the window close


class ConfigDialog(QtWidgets.QDialog):
    """A dialog that allows the user to configure the VRC Speech Assistant application.

    Attributes
    ----------
    polly: providers.Polly
        The Amazon Polly provider object.
    """

    def __init__(self, parent: VRCSpeechAssistantWindow):
        """Constructor for ConfigDialog.

        Parameters
        ----------
        parent : VRCSpeechAssistantWindow
            The main VRC Speech Assistant application.
        """
        super().__init__()
        self.setWindowTitle("Edit configuration")
        self.setWindowIcon(QtGui.QIcon(str(RESOURCES_PATH.joinpath("logo.png"))))
        self.parent = parent
        self.polly = providers.Polly()
        self._init_ui()

    def _init_ui(self):
        # Main layout.
        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.setSpacing(10)
        main_layout.setContentsMargins(30, 30, 30, 30)
        # Deepl widget.
        deepl_api_key_label = QtWidgets.QLabel("DeepL API Key", self)
        self.deepl_api_key_line_edit = QtWidgets.QLineEdit(self)
        self.deepl_api_key_line_edit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.deepl_api_key_line_edit.setText(self.parent.config["API"]["deepl_api_key"])
        # Amazon AWS widget.
        aws_access_key_id_label = QtWidgets.QLabel("Amazon AWS access key ID", self)
        self.aws_access_key_id_line_edit = QtWidgets.QLineEdit(self)
        self.aws_access_key_id_line_edit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.aws_access_key_id_line_edit.setText(self.polly.aws_credentials["default"]["aws_access_key_id"])
        aws_secret_access_key_label = QtWidgets.QLabel("Amazon AWS secret key", self)
        self.aws_secret_access_key_line_edit = QtWidgets.QLineEdit(self)
        self.aws_secret_access_key_line_edit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.aws_secret_access_key_line_edit.setText(
            self.polly.aws_credentials["default"]["aws_secret_access_key"]
        )
        # Button box.
        ok_button = QtWidgets.QPushButton("Ok")
        ok_button.clicked.connect(self._accept)
        # Add widgets to main layout.
        main_layout.addWidget(deepl_api_key_label)
        main_layout.addWidget(self.deepl_api_key_line_edit)
        main_layout.addWidget(aws_access_key_id_label)
        main_layout.addWidget(self.aws_access_key_id_line_edit)
        main_layout.addWidget(aws_secret_access_key_label)
        main_layout.addWidget(self.aws_secret_access_key_line_edit)
        main_layout.addWidget(ok_button)

    def _accept(self):
        # Removing all whitespaces from the text in the line edits.
        deepl_key = re.sub(r"\s+", "", self.deepl_api_key_line_edit.text())
        aws_access_key_id = re.sub(r"\s+", "", self.aws_access_key_id_line_edit.text())
        aws_secret_access_key = re.sub(r"\s+", "", self.aws_secret_access_key_line_edit.text())
        # Update configs.
        self.parent.config["API"]["deepl_api_key"] = deepl_key
        self.parent.save_config()
        self.polly.aws_credentials["default"]["aws_access_key_id"] = aws_access_key_id
        self.polly.aws_credentials["default"]["aws_secret_access_key"] = aws_secret_access_key
        self.polly.aws_credentials.write(self.polly.AWS_CREDENTIALS_FILE.open("w", encoding="utf-8"))
        # Tests.
        if self.deepl_api_key_line_edit.text():
            if not utils.test_deepl(self.deepl_api_key_line_edit.text()):
                self._disable_translator()
                return
            self.parent.log_line_edit.clear()
            self.parent.translator = deepl.Translator(self.deepl_api_key_line_edit.text())
            self.parent.target_language_combo.setEnabled(True)
        self.deepl_api_key_line_edit.setStyleSheet("")
        if self.aws_access_key_id_line_edit.text() and self.aws_secret_access_key_line_edit.text():
            if not providers.test_amazon_polly():
                self.aws_access_key_id_line_edit.setStyleSheet("background-color: red")
                self.aws_secret_access_key_line_edit.setStyleSheet("background-color: red")
                self.parent.log_line_edit.setText("Amazon AWS credentials are invalid or missing.")
                self.parent.toggle_activate_button.setEnabled(False)
                return
            self.parent.log_line_edit.clear()
            self.parent.toggle_activate_button.setEnabled(True)
        self.aws_access_key_id_line_edit.setStyleSheet("")
        self.aws_secret_access_key_line_edit.setStyleSheet("")
        # Close the dialog.
        self.close()

    def _disable_translator(self):
        self.deepl_api_key_line_edit.setStyleSheet("background-color: red")
        self.parent.log_line_edit.setText("DeepL API key is invalid or account usage limit reached.")
        self.parent.translator = None
        self.parent.target_language_combo.setEnabled(False)
        self.parent.target_language_combo.setCurrentText(self.parent.source_language_combo.currentText())
        self.parent.config["SPEECH"]["target_language"] = self.parent.source_language_combo.currentText()
        self.parent.save_config()


if __name__ == "__main__":
    app = QtWidgets.QApplication()
    darkstyle.apply(app)
    vrc_speech_assistant_window = VRCSpeechAssistantWindow()
    vrc_speech_assistant_window.show()
    sys.exit(app.exec())
