"""Initialize package."""
from vrchat_speech_assistant.version import __version__
from logger import set_level

set_level("INFO")
